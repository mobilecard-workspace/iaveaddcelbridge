/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.addcel.iave.model.vo;

/**
 *
 * @author JCDP
 */

public class CompraIAVE {

    private String login;
    private String password;
    private String cvv2;
    private String vigencia;
    private String producto;
    private String tarjeta;
    private String pin;
    private String imei;
    private String cx;
    private String cy;
    private String tipo;
    private String software;
    private String modelo;
    private String key;
    
    private int idAplicacion;
    private long idUsuario;
    private String nombre;
    private String apellido;
    private String materno;
    private String email;
    private String tarjetaBanco;
    private int tipoTarjeta;
    
    public String getCx() {
        return cx;
    }
    public void setCx(String cx) {
        this.cx = cx;
    }

   public String getCy() {
        return cy;
    }
    public void setCy(String cy) {
        this.cy = cy;
    }

    /**
     * @return the login
     */
    public String getLogin() {
        return login;
    }

    /**
     * @param login the login to set
     */
    public void setLogin(String login) {
        this.login = login;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the cvv2
     */
    public String getCvv2() {
        return cvv2;
    }

    /**
     * @param cvv2 the cvv2 to set
     */
    public void setCvv2(String cvv2) {
        this.cvv2 = cvv2;
    }

    /**
     * @return the vigencia
     */
    public String getVigencia() {
        return vigencia;
    }

    /**
     * @param vigencia the vigencia to set
     */
    public void setVigencia(String vigencia) {
        this.vigencia = vigencia;
    }

    /**
     * @return the producto
     */
    public String getProducto() {
        return producto;
    }

    /**
     * @param producto the producto to set
     */
    public void setProducto(String producto) {
        this.producto = producto;
    }

    /**
     * @return the tarjeta
     */
    public String getTarejeta() {
        return tarjeta;
    }

    /**
     * @param telefono the tarjeta to set
     */
    public void setTarjeta(String tarjeta) {
        this.tarjeta = tarjeta;
    }
    /**
     * @return the pin
     */
    public String getPin() {
        return pin;
    }

    /**
     * @param telefono the Pin to set
     */
    public void setPin(String pin) {
        this.pin = pin;
    }
    
    /**
     * @return the imei
     */
    public String getImei() {
        return imei;
    }

    /**
     * @param imei the imei to set
     */
    public void setImei(String imei) {
        this.imei = imei;
    }     
    
   public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getSoftware() {
        return software;
    }

    public void setSoftware(String software) {
        this.software = software;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public int getIdAplicacion() {
        return idAplicacion;
    }
    public void setIdAplicacion(int idAplicacion) {
        this.idAplicacion = idAplicacion;
    }
    public long getIdUsuario() {
        return idUsuario;
    }
    public void setIdUsuario(long idUsuario) {
        this.idUsuario = idUsuario;
    }
    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public String getApellido() {
        return apellido;
    }
    public void setApellido(String apellido) {
        this.apellido = apellido;
    }
    public String getMaterno() {
        return materno;
    }
    public void setMaterno(String materno) {
        this.materno = materno;
    }
    public String getTarjetaBanco() {
        return tarjetaBanco;
    }
    public void setTarjetaBanco(String tarjetaBanco) {
        this.tarjetaBanco = tarjetaBanco;
    }  
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public int getTipoTarjeta() {
        return tipoTarjeta;
    }
    public void setTipoTarjeta(int tipoTarjeta) {
        this.tipoTarjeta = tipoTarjeta;
    }  

}
