package com.addcel.iave.model.vo;

public class ProsaResponse {

	public String error="0";
	public String msg;
    
    private boolean isAuthorized = false;
    private boolean isRejected = false;
    private boolean isProsaError = false;
    
    private String transaccion;
    private  String seguridad;

    private String transactionId;
    private String bancoAdquirente;
    private String nombreComercio;
    private String direccionComercio;
    private String afiliacion;
    private String importe;
    private String moneda;
    private String fechaHora;
    private String claveOperacion;
    private String numeroCaja;
    private String producto;
    private String bancoEmisor;
    private String marca;
    private String autorizacion;
    private String poblacion;
    
    private String claveRechazo;
    private String descripcionRechazo;
       
                

    public ProsaResponse(String error, String msg) {
        this.error = error;
        this.msg = msg;
    }
    
    public ProsaResponse()
    {
        
    }

    @Override
    public String toString() {
        return "code:"+error+"  msg:"+msg;
    }

    /**
     * @return the isAuthorized
     */
    public boolean isIsAuthorized() {
        return isAuthorized;
    }

    public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}
    
    /**
     * @param isAuthorized the isAuthorized to set
     */
    public void setIsAuthorized(boolean isAuthorized) {
        this.isAuthorized = isAuthorized;
    }

    /**
     * @return the isRejected
     */
    public boolean isIsRejected() {
        return isRejected;
    }

    /**
     * @param isRejected the isRejected to set
     */
    public void setIsRejected(boolean isRejected) {
        this.isRejected = isRejected;
    }

    /**
     * @return the isProsaError
     */
    public boolean isIsProsaError() {
        return isProsaError;
    }

    /**
     * @param isProsaError the isProsaError to set
     */
    public void setIsProsaError(boolean isProsaError) {
        this.isProsaError = isProsaError;
    }

    /**
     * @return the transaccion
     */
    public String getTransaccion() {
        return transaccion;
    }

    /**
     * @param transaccion the transaccion to set
     */
    public void setTransaccion(String transaccion) {
        this.transaccion = transaccion;
    }

    /**
     * @return the seguridad
     */
    public String getSeguridad() {
        return seguridad;
    }

    /**
     * @param seguridad the seguridad to set
     */
    public void setSeguridad(String seguridad) {
        this.seguridad = seguridad;
    }

    /**
     * @return the bancoAdquirente
     */
    public String getBancoAdquirente() {
        return bancoAdquirente;
    }

    /**
     * @param bancoAdquirente the bancoAdquirente to set
     */
    public void setBancoAdquirente(String bancoAdquirente) {
        this.bancoAdquirente = bancoAdquirente;
    }

    /**
     * @return the nombreComercio
     */
    public String getNombreComercio() {
        return nombreComercio;
    }

    /**
     * @param nombreComercio the nombreComercio to set
     */
    public void setNombreComercio(String nombreComercio) {
        this.nombreComercio = nombreComercio;
    }

    /**
     * @return the direccionComercio
     */
    public String getDireccionComercio() {
        return direccionComercio;
    }

    /**
     * @param direccionComercio the direccionComercio to set
     */
    public void setDireccionComercio(String direccionComercio) {
        this.direccionComercio = direccionComercio;
    }

    /**
     * @return the afiliacion
     */
    public String getAfiliacion() {
        return afiliacion;
    }

    /**
     * @param afiliacion the afiliacion to set
     */
    public void setAfiliacion(String afiliacion) {
        this.afiliacion = afiliacion;
    }

    /**
     * @return the importe
     */
    public String getImporte() {
        return importe;
    }

    /**
     * @param importe the importe to set
     */
    public void setImporte(String importe) {
        this.importe = importe;
    }

    /**
     * @return the moneda
     */
    public String getMoneda() {
        return moneda;
    }

    /**
     * @param moneda the moneda to set
     */
    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    /**
     * @return the fechaHora
     */
    public String getFechaHora() {
        return fechaHora;
    }

    /**
     * @param fechaHora the fechaHora to set
     */
    public void setFechaHora(String fechaHora) {
        this.fechaHora = fechaHora;
    }

    /**
     * @return the claveOperacion
     */
    public String getClaveOperacion() {
        return claveOperacion;
    }

    /**
     * @param claveOperacion the claveOperacion to set
     */
    public void setClaveOperacion(String claveOperacion) {
        this.claveOperacion = claveOperacion;
    }

    /**
     * @return the numeroCaja
     */
    public String getNumeroCaja() {
        return numeroCaja;
    }

    /**
     * @param numeroCaja the numeroCaja to set
     */
    public void setNumeroCaja(String numeroCaja) {
        this.numeroCaja = numeroCaja;
    }

    /**
     * @return the producto
     */
    public String getProducto() {
        return producto;
    }

    /**
     * @param producto the producto to set
     */
    public void setProducto(String producto) {
        this.producto = producto;
    }

    /**
     * @return the bancoEmisor
     */
    public String getBancoEmisor() {
        return bancoEmisor;
    }

    /**
     * @param bancoEmisor the bancoEmisor to set
     */
    public void setBancoEmisor(String bancoEmisor) {
        this.bancoEmisor = bancoEmisor;
    }

    /**
     * @return the marca
     */
    public String getMarca() {
        return marca;
    }

    /**
     * @param marca the marca to set
     */
    public void setMarca(String marca) {
        this.marca = marca;
    }

    /**
     * @return the autorizacion
     */
    public String getAutorizacion() {
        return autorizacion;
    }

    /**
     * @param autorizacion the autorizacion to set
     */
    public void setAutorizacion(String autorizacion) {
        this.autorizacion = autorizacion;
    }

    /**
     * @return the poblacion
     */
    public String getPoblacion() {
        return poblacion;
    }

    /**
     * @param poblacion the poblacion to set
     */
    public void setPoblacion(String poblacion) {
        this.poblacion = poblacion;
    }

    /**
     * @return the claveRechazo
     */
    public String getClaveRechazo() {
        return claveRechazo;
    }

    /**
     * @param claveRechazo the claveRechazo to set
     */
    public void setClaveRechazo(String claveRechazo) {
        this.claveRechazo = claveRechazo;
    }

    /**
     * @return the descripcionRechazo
     */
    public String getDescripcionRechazo() {
        return descripcionRechazo;
    }

    /**
     * @param descripcionRechazo the descripcionRechazo to set
     */
    public void setDescripcionRechazo(String descripcionRechazo) {
        this.descripcionRechazo = descripcionRechazo;
    }

    /**
     * @return the transactionId
     */
    public String getTransactionId() {
        return transactionId;
    }

    /**
     * @param transactionId the transactionId to set
     */
    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }
	
}
