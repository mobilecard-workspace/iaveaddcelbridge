package com.addcel.iave.model.vo;

import java.math.BigDecimal;

public class ProveedorVO {
    private Long id_proveedor;

    private Long prv_num_cliente;

    private Long prv_pos_id;

    private String prv_nombre_comercio;

    private String prv_domicilio;

    private String prv_dealer_login;

    private String prv_dealer_password;

    private String prv_pos_login;

    private String prv_pos_password;

    private String prv_nombre_completo;

    private String prv_clave_personal;

    private Integer prv_status;

    private String prv_claveWS;

    private String prv_path;

    private Integer id_categoria;

    private BigDecimal comision;

    private Integer compatible_Amex;

    private Integer compatible_Visa;

    private BigDecimal COMISION_PORCENTAJE;

    private BigDecimal MIN_COMISION_PORCENTAJE;

    private BigDecimal MIN_COMISION_FIJA;

    private BigDecimal tipo_cambio;

    private BigDecimal MAX_TRANSACCION;

    private String AFILIACION;

    public Long getId_proveedor() {
        return id_proveedor;
    }

    public void setId_proveedor(Long id_proveedor) {
        this.id_proveedor = id_proveedor;
    }

    public Long getPrv_num_cliente() {
        return prv_num_cliente;
    }

    public void setPrv_num_cliente(Long prv_num_cliente) {
        this.prv_num_cliente = prv_num_cliente;
    }

    public Long getPrv_pos_id() {
        return prv_pos_id;
    }

    public void setPrv_pos_id(Long prv_pos_id) {
        this.prv_pos_id = prv_pos_id;
    }

    public String getPrv_nombre_comercio() {
        return prv_nombre_comercio;
    }

    public void setPrv_nombre_comercio(String prv_nombre_comercio) {
        this.prv_nombre_comercio = prv_nombre_comercio;
    }

    public String getPrv_domicilio() {
        return prv_domicilio;
    }

    public void setPrv_domicilio(String prv_domicilio) {
        this.prv_domicilio = prv_domicilio;
    }

    public String getPrv_dealer_login() {
        return prv_dealer_login;
    }

    public void setPrv_dealer_login(String prv_dealer_login) {
        this.prv_dealer_login = prv_dealer_login;
    }

    public String getPrv_dealer_password() {
        return prv_dealer_password;
    }

    public void setPrv_dealer_password(String prv_dealer_password) {
        this.prv_dealer_password = prv_dealer_password;
    }

    public String getPrv_pos_login() {
        return prv_pos_login;
    }

    public void setPrv_pos_login(String prv_pos_login) {
        this.prv_pos_login = prv_pos_login;
    }

    public String getPrv_pos_password() {
        return prv_pos_password;
    }

    public void setPrv_pos_password(String prv_pos_password) {
        this.prv_pos_password = prv_pos_password;
    }

    public String getPrv_nombre_completo() {
        return prv_nombre_completo;
    }

    public void setPrv_nombre_completo(String prv_nombre_completo) {
        this.prv_nombre_completo = prv_nombre_completo;
    }

    public String getPrv_clave_personal() {
        return prv_clave_personal;
    }

    public void setPrv_clave_personal(String prv_clave_personal) {
        this.prv_clave_personal = prv_clave_personal;
    }

    public Integer getPrv_status() {
        return prv_status;
    }

    public void setPrv_status(Integer prv_status) {
        this.prv_status = prv_status;
    }

    public String getPrv_claveWS() {
        return prv_claveWS;
    }

    public void setPrv_claveWS(String prv_claveWS) {
        this.prv_claveWS = prv_claveWS;
    }

    public String getPrv_path() {
        return prv_path;
    }

    public void setPrv_path(String prv_path) {
        this.prv_path = prv_path;
    }

    public Integer getId_categoria() {
        return id_categoria;
    }

    public void setId_categoria(Integer id_categoria) {
        this.id_categoria = id_categoria;
    }

    public BigDecimal getComision() {
        return comision;
    }

    public void setComision(BigDecimal comision) {
        this.comision = comision;
    }

    public Integer getCompatible_Amex() {
        return compatible_Amex;
    }

    public void setCompatible_Amex(Integer compatible_Amex) {
        this.compatible_Amex = compatible_Amex;
    }

    public Integer getCompatible_Visa() {
        return compatible_Visa;
    }

    public void setCompatible_Visa(Integer compatible_Visa) {
        this.compatible_Visa = compatible_Visa;
    }

    public BigDecimal getCOMISION_PORCENTAJE() {
        return COMISION_PORCENTAJE;
    }

    public void setCOMISION_PORCENTAJE(BigDecimal COMISION_PORCENTAJE) {
        this.COMISION_PORCENTAJE = COMISION_PORCENTAJE;
    }

    public BigDecimal getMIN_COMISION_PORCENTAJE() {
        return MIN_COMISION_PORCENTAJE;
    }

    public void setMIN_COMISION_PORCENTAJE(BigDecimal MIN_COMISION_PORCENTAJE) {
        this.MIN_COMISION_PORCENTAJE = MIN_COMISION_PORCENTAJE;
    }

    public BigDecimal getMIN_COMISION_FIJA() {
        return MIN_COMISION_FIJA;
    }

    public void setMIN_COMISION_FIJA(BigDecimal MIN_COMISION_FIJA) {
        this.MIN_COMISION_FIJA = MIN_COMISION_FIJA;
    }

    public BigDecimal getTipo_cambio() {
        return tipo_cambio;
    }

    public void setTipo_cambio(BigDecimal tipo_cambio) {
        this.tipo_cambio = tipo_cambio;
    }

    public BigDecimal getMAX_TRANSACCION() {
        return MAX_TRANSACCION;
    }

    public void setMAX_TRANSACCION(BigDecimal MAX_TRANSACCION) {
        this.MAX_TRANSACCION = MAX_TRANSACCION;
    }

    public String getAFILIACION() {
        return AFILIACION;
    }

    public void setAFILIACION(String AFILIACION) {
        this.AFILIACION = AFILIACION;
    }
}