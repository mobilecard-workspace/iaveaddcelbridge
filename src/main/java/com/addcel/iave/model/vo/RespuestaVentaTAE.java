/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.addcel.iave.model.vo;

/**
 *
 * @author marco
 */
public class RespuestaVentaTAE {
    
    String region="";
    String transactionno="";
    String company="";
    String store="";
    String terminal="";
    String amount="";
    String phone="";
    String referenceid="";
    String telcelid="";
    String responsetelcel="";
    String sendtime="";
    String responsetime="";
    String status="";
    String nointentos="";
    String code ="";
    String error ="";


            
    public String getErro() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    
    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getNointentos() {
        return nointentos;
    }

    public void setNointentos(String nointentos) {
        this.nointentos = nointentos;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getReferenceid() {
        return referenceid;
    }

    public void setReferenceid(String referenceid) {
        this.referenceid = referenceid;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getResponsetelcel() {
        return responsetelcel;
    }

    public void setResponsetelcel(String responsetelcel) {
        this.responsetelcel = responsetelcel;
    }

    public String getResponsetime() {
        return responsetime;
    }

    public void setResponsetime(String responsetime) {
        this.responsetime = responsetime;
    }

    public String getSendtime() {
        return sendtime;
    }

    public void setSendtime(String sendtime) {
        this.sendtime = sendtime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStore() {
        return store;
    }

    public void setStore(String store) {
        this.store = store;
    }

    public String getTelcelid() {
        return telcelid;
    }

    public void setTelcelid(String telcelid) {
        this.telcelid = telcelid;
    }

    public String getTerminal() {
        return terminal;
    }

    public void setTerminal(String terminal) {
        this.terminal = terminal;
    }

    public String getTransactionno() {
        return transactionno;
    }

    public void setTransactionno(String transactionno) {
        this.transactionno = transactionno;
    }

    
}


