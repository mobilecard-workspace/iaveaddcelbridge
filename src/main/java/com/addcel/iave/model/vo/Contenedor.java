/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.addcel.iave.model.vo;

import java.util.List;

/**
 *
 * @author david
 */
public class Contenedor {
    private List<Bitacora> compras;
    private List<Producto> productos;
    private List<Proveedor> proveedores;
    
    /**
     * @return the productos
     */
    public List<Producto> getProductos() {
        return productos;
    }

    /**
     * @param productos the productos to set
     */
    public void setProductos(List<Producto> productos) {
        this.productos = productos;
    }
    
    /**
     * @return the compras
     */
    public List<Bitacora> getCompras() {
        return compras;
    }

    /**
     * @param compras the compras to set
     */
    public void setCompras(List<Bitacora> compras) {
        this.compras = compras;
    }
    
    public List<Proveedor> getProveedores() {
        return proveedores;
    }

    public void setProveedores(List<Proveedor> proveedores) {
        this.proveedores = proveedores;
    }
}
