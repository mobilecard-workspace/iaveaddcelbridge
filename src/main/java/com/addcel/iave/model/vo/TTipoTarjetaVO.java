package com.addcel.iave.model.vo;

import java.util.List;

public class TTipoTarjetaVO {
    private Integer id_tipo_tarjeta;

    private String desc_tipo_tarjeta;

    private Integer activo;
    
    public List<TTipoTarjetaVO> getTarjetas() {
		return tarjetas;
	}

	public void setTarjetas(List<TTipoTarjetaVO> tarjetas) {
		this.tarjetas = tarjetas;
	}

	private List<TTipoTarjetaVO> tarjetas;

    public Integer getId_tipo_tarjeta() {
        return id_tipo_tarjeta;
    }

    public void setId_tipo_tarjeta(Integer id_tipo_tarjeta) {
        this.id_tipo_tarjeta = id_tipo_tarjeta;
    }

    public String getDesc_tipo_tarjeta() {
        return desc_tipo_tarjeta;
    }

    public void setDesc_tipo_tarjeta(String desc_tipo_tarjeta) {
        this.desc_tipo_tarjeta = desc_tipo_tarjeta;
    }

    public Integer getActivo() {
        return activo;
    }

    public void setActivo(Integer activo) {
        this.activo = activo;
    }
}