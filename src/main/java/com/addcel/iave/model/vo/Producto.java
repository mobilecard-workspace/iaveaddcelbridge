/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.addcel.iave.model.vo;

import java.util.List;

/**
 *
 * @author -
 */
public class Producto {

    private String clave;
    private String claveWS;
    private String proveedor;
    private String descripcion;
    private String monto;
    private String path;
    private String nombre;

        /**
     * @return the clave
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param 
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the clave
     */
    public String getClave() {
        return clave;
    }

    /**
     * @param clave the clave to set
     */
    public void setClave(String clave) {
        this.clave = clave;
    }

    /**
     * @return the claveWS
     */
    public String getClaveWS() {
        return claveWS;
    }

    /**
     * @param claveWS the claveWS to set
     */
    public void setClaveWS(String claveWS) {
        this.claveWS = claveWS;
    }

    /**
     * @return the proveedor
     */
    public String getProveedor() {
        return proveedor;
    }

    /**
     * @param proveedor the proveedor to set
     */
    public void setProveedor(String proveedor) {
        this.proveedor = proveedor;
    }

    /**
     * @return the descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * @param descripcion the descripcion to set
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * @return the monto
     */
    public String getMonto() {
        return monto;
    }

    /**
     * @param monto the monto to set
     */
    public void setMonto(String monto) {
        this.monto = monto;
    }

    /**
     * @return the path
     */
    public String getPath() {
        return path;
    }

    /**
     * @param path the path to set
     */
    public void setPath(String path) {
        this.path = path;
    }
}
