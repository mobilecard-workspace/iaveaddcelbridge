package com.addcel.iave.model.vo;

import java.util.Date;

public class BitacoraprosaVO {
    private Integer id_bitacoraProsa;

    private Long id_bitacora;

    private Long id_usuario;

    private String tarjeta;

    private String transaccion;

    private String autorizacion;

    private Date fecha;

    private Date bit_hora;

    private String tarjetaIAVE;

    private Long cargo;

    private Long comision;

    private String cx;

    private String cy;

    private String concepto;

    public Integer getId_bitacoraProsa() {
        return id_bitacoraProsa;
    }

    public void setId_bitacoraProsa(Integer id_bitacoraProsa) {
        this.id_bitacoraProsa = id_bitacoraProsa;
    }

    public Long getId_bitacora() {
        return id_bitacora;
    }

    public void setId_bitacora(Long id_bitacora) {
        this.id_bitacora = id_bitacora;
    }

    public Long getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(Long id_usuario) {
        this.id_usuario = id_usuario;
    }

    public String getTarjeta() {
        return tarjeta;
    }

    public void setTarjeta(String tarjeta) {
        this.tarjeta = tarjeta;
    }

    public String getTransaccion() {
        return transaccion;
    }

    public void setTransaccion(String transaccion) {
        this.transaccion = transaccion;
    }

    public String getAutorizacion() {
        return autorizacion;
    }

    public void setAutorizacion(String autorizacion) {
        this.autorizacion = autorizacion;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Date getBit_hora() {
        return bit_hora;
    }

    public void setBit_hora(Date bit_hora) {
        this.bit_hora = bit_hora;
    }

    public String getTarjetaIAVE() {
        return tarjetaIAVE;
    }

    public void setTarjetaIAVE(String tarjetaIAVE) {
        this.tarjetaIAVE = tarjetaIAVE;
    }

    public Long getCargo() {
        return cargo;
    }

    public void setCargo(Long cargo) {
        this.cargo = cargo;
    }

    public Long getComision() {
        return comision;
    }

    public void setComision(Long comision) {
        this.comision = comision;
    }

    public String getCx() {
        return cx;
    }

    public void setCx(String cx) {
        this.cx = cx;
    }

    public String getCy() {
        return cy;
    }

    public void setCy(String cy) {
        this.cy = cy;
    }

    public String getConcepto() {
        return concepto;
    }

    public void setConcepto(String concepto) {
        this.concepto = concepto;
    }
}