package com.addcel.iave.servicios.services;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.ModelAndView;

import com.addcel.iave.model.mapper.IaveServiciosMapper;
import com.addcel.iave.model.vo.ProcomVO;
import com.addcel.iave.model.vo.RespuestaServicioVO;
import com.addcel.iave.model.vo.UsuarioVO;

@Service
public class PagosService {
	private static final Logger logger = Logger.getLogger(PagosService.class);

	@Autowired
	private IaveServiciosMapper mapper;

	public PagosService() {

	}

	public ModelAndView pagoServicios3DS(long idUsuario, double monto, long idBitacora) {
		ModelAndView mav = null;

		UsuarioVO usuarioVO = null;
		ProcomVO procom = null;
		RespuestaServicioVO respuesta = null;

		try {
			respuesta = new RespuestaServicioVO();

			// busco al usuario.
			//usuarioVO = mapper.getUsuario(idUsuario);

			if (usuarioVO == null) {
				respuesta.setIdError(-2);
				respuesta.setMsgError("Error al consultar usuario -2");
				logger.error("Error : Error al consultar usuario -2");

			} else if (usuarioVO.getId_usr_status() == 0) {
				respuesta.setIdError(-6);
				respuesta.setMsgError("Usuario Inactivo. Consulte con el Administrador");
				logger.error("Error : Usuario Inactivo. Consulte con el Administrador");

			} else if (usuarioVO.getId_usr_status() == 99) {
				respuesta.setIdError(-7);
				respuesta.setMsgError("Modifique su Informacion por seguridad.");
				logger.error("Error : Modifique su Informacion por seguridad.");

			} else if (usuarioVO.getId_usr_status() == 98) {
				respuesta.setIdError(-8);
				respuesta.setMsgError("Modifique su clave de acceso por seguridad.");
				logger.error("Error : Modifique su clave de acceso por seguridad.");

			} else if (usuarioVO.getId_usr_status() == 1) {

				// REALIZO EL COBRO
				if (usuarioVO.getId_tipo_tarjeta() == 1 || usuarioVO.getId_tipo_tarjeta() == 2) {

					logger.debug("Pre - Monto: " + monto);
					logger.debug("Pre - Referencia: " + idBitacora);

					procom = new ProcomService().comercioFin(idBitacora, monto);

				} else if (usuarioVO.getId_tipo_tarjeta() == 3) {
					respuesta.setIdError(-1);
					respuesta.setMsgError(
							"Disculpe las molestias, continuamos trabajando para habilitar pagos con tarjetas AMEX.");
					logger.error("Error Intento de pago AMEX.");

				} else {
					respuesta.setIdError(-1);
					respuesta.setMsgError("Tipo de Tarjeta no valida.");
					logger.error("Error tipo de tarjeta registrada: {}" + usuarioVO.getId_tipo_tarjeta());
				}
			} else {
				respuesta.setIdError(-1);
				respuesta.setMsgError("Error en MOBILECARD, problemas internos -2");
				logger.error("Error al insertar en la bitacora");
			}

			// jsonResp=utilService.objectToJson(respuesta);
		} catch (Exception e) {
			respuesta.setIdError(-100);
			respuesta.setMsgError("Ocurrio un error al realizar el pago del servicio, problemas internos -3");
			logger.error("Error al realizar la recarga", e);
		} finally {
			if (respuesta.getIdError() == 0) {

				mav = new ModelAndView("comerciofin");

				mav.addObject("prosa", procom);

			} else {
				mav = new ModelAndView("error");
				mav.addObject("descError", respuesta.getMsgError());
			}
		}

		return mav;
	}

	@SuppressWarnings("unchecked")
	public ModelAndView procesaRespuestaRecargaProsa(String EM_Response, String EM_Total, String EM_OrderID,
			String EM_Merchant, String EM_Store, String EM_Term, String EM_RefNum, String EM_Auth, String EM_Digest) {
		ModelAndView mav = null;

		RespuestaServicioVO respuesta = null;
		UsuarioVO usuarioVO = null;
		Double comision = null;

		try {
			logger.debug("EM_Response: " + EM_Response);
			logger.debug("EM_Total: " + EM_Total);
			logger.debug("EM_OrderID: " + EM_OrderID);
			logger.debug("EM_Merchant: " + EM_Merchant);
			logger.debug("EM_RefNum: " + EM_RefNum);
			logger.debug("EM_Auth: " + EM_Auth);

			respuesta = new RespuestaServicioVO();

			if (!EM_Auth.equals("000000") || EM_Auth.equals("A")) {
				// TODO transaccion aprobada, aprovisionar el servicio
				logger.debug("Transaccion Aprobada");
				
				mav = new ModelAndView("pago");
				mav.addObject("respuesta", "Aprovado");
				mav.addObject("referencia", EM_OrderID);				
				mav.addObject("autorizacion", EM_Auth);															
				mav.addObject("total", EM_Total);				
			
			} else {
				respuesta.setIdError(-9);
				respuesta.setMsgError("Tarjeta rechazada, " + EM_Response + " - " + EM_RefNum);
				logger.error("Error Pago 3D Secure: " + EM_Response + " - " + EM_RefNum);

			}

		} catch (Exception e) {
			respuesta.setIdError(-100);
			respuesta.setMsgError("Ocurrio un error, problemas internos -3");
			logger.error("Error al realizar la recarga", e);
		} finally {
			if (respuesta.getIdError() != 0) {
				mav = new ModelAndView("error");
				mav.addObject("descError", respuesta.getMsgError());
			}
		}
		return mav;

	}
}