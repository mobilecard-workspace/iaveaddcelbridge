/**
 * WsPrepagoRecargas.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.iave.ws.clientes.iavePrepago;

public interface WsPrepagoRecargas extends javax.xml.rpc.Service {
    public java.lang.String getwsPrepagoRecargasSoapAddress();

    public com.addcel.iave.ws.clientes.iavePrepago.WsPrepagoRecargasSoap getwsPrepagoRecargasSoap() throws javax.xml.rpc.ServiceException;

    public com.addcel.iave.ws.clientes.iavePrepago.WsPrepagoRecargasSoap getwsPrepagoRecargasSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
