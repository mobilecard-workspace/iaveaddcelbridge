/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.addcel.iave.business.amex;

import com.addcel.iave.model.vo.RespuestaAmex;

/**
 *
 * @author JCDP
 */
public interface PagoAmex {
    
    /**
     * M&eacute;todo que se comunica con el servlet que permite realizar los pagos por American Express
     * @param tarjeta Los 16 d&iacute;gitos de la tarjeta
     * @param vigencia Fecha de expiraci&oacute;n de la tarjeta
     * @param monto Monto a cobrar
     * @param cvv2 N&uacute;mero de seguridad de la tarjeta
     * @param direccion Domicilio del tarjetahabiente
     * @param codigoPostal C&oacute;digo postal del domicilio del tarjetahabiente
     * @return Respuesta del servlet de compra
     */
    RespuestaAmex realizarPago(String tarjeta, String vigencia, String monto, String cvv2, String direccion, String codigoPostal);
    
    RespuestaAmex realizarPagoTelepeaje(String tarjeta, String vigencia, String monto, String cvv2, String direccion, String codigoPostal, String producto);
    
//    RespuestaAmex authorizeByUserId(String idUsuario, String pass, String afiliacion, String cvv2, String monto, String codigoPostal);
}
